const mongoose = require('mongoose');

const enums = {
  status: ['Active', 'Archived', 'Deleted']
};

const ProjectSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  status: {
    type: String,
    enum: enums.status,
    required: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  todo: {
    type: String,
    required: true,
    trim: true
  }
});

module.exports = mongoose.model('Project', ProjectSchema);
