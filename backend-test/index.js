const express = require('express');
const mongoose = require('mongoose');
const argon2 = require('argon2');

const cors = require('cors');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const UserSchema = require('./src/models/User/UserSchema');
const ProjectSchema = require('./src/models/Project/ProjectSchema');

(() => {
  try {
    mongoose.connect(`${process.env.DB_URL}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    const app = express();
    app.listen(5000);
    app.use(express.json());
    app.use(cookieParser());
    app.use(
      cors({
        origin: process.env.FRONTEND_URL ?? `${process.env.FRONTEND_URL}`,
        optionsSuccessStatus: 200,
        credentials: true
      })
    );

    app.get('/users', async (req, res, next) => {
      try {
        const users = await UserSchema.find();
        return res.send(users);
      } catch (err) {
        return next(err);
      }
    });

    app.get('/users/logout', (req, res, next) => {
      try {
        return res.clearCookie('access_token').sendStatus(200);
      } catch (err) {
        return next(err);
      }
    });

    app.get('/users/:id', async (req, res, next) => {
      try {
        const user = await UserSchema.findById({ _id: req.params.id });
        return res.send(user);
      } catch (err) {
        return next(err);
      }
    });

    app.put('/users/:id', async (req, res, next) => {
      try {
        await UserSchema.updateOne(
          { _id: req.params.id },
          { ...req.body, _id: req.params.id }
        );
        return res.status(200).send({ message: 'User modified' });
      } catch (err) {
        return next(err);
      }
    });

    app.delete('/users/:id', async (req, res, next) => {
      try {
        await UserSchema.deleteOne({ _id: req.params.id });
        return res.send({ message: 'Deleted User !' });
      } catch (err) {
        return next(err);
      }
    });

    app.post('/users/register', async (req, res, next) => {
      try {
        const { email, password, firstName, lastName } = req.body;
        const user = new UserSchema(req.body);

        if (!firstName || !lastName || !email || !password) {
          res.status(400).send({ error: 'All fields must be completed' });
          return;
        }

        try {
          const hash = await argon2.hash(password);
          user.password = hash;
          await user.save();
          res.send(user);
        } catch (error) {
          res.status(500).send({
            error: error.message
          });
        }
      } catch (err) {
        next(err);
      }
    });

    app.post('/users/login', async (req, res, next) => {
      try {
        const { email, password } = req.body;

        if (!email || !password) {
          res.status(401).send({ error: 'Please specify credentials' });
          return;
        }

        try {
          const user = await UserSchema.find({ email });
          const hash = user[0].password;
          if (await argon2.verify(hash, password)) {
            const token = jwt.sign(
              { email, password },
              process.env.JWT_AUTH_SECRET,
              { expiresIn: '24h' }
            );

            res
              .status(200)
              .cookie('access_token', token, {
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production'
              })
              .send({
                email,
                password
              });
          }
          res.status(401).send({ error: 'Please specify credentials' });
        } catch (error) {
          res.status(500).send({
            error: error.message
          });
        }
      } catch (err) {
        next(err);
      }
    });

    app.get('/projects', async (req, res, next) => {
      try {
        const projects = await ProjectSchema.find();
        return res.send(projects);
      } catch (err) {
        return next(err);
      }
    });

    app.get('/projects/:id', async (req, res, next) => {
      try {
        const project = await ProjectSchema.findById({ _id: req.params.id });
        return res.send(project);
      } catch (err) {
        return next(err);
      }
    });

    app.put('/projects/:id', async (req, res, next) => {
      try {
        await ProjectSchema.updateOne(
          { _id: req.params.id },
          { ...req.body, _id: req.params.id }
        );
        return res.status(200).json({ message: 'Modified project !' });
      } catch (err) {
        return next(err);
      }
    });

    app.delete('/projects/:id', async (req, res, next) => {
      try {
        await ProjectSchema.deleteOne({ _id: req.params.id });
        return res.status(200).json({ message: 'Project deleted' });
      } catch (err) {
        return next(err);
      }
    });

    app.post('/projects', async (req, res, next) => {
      try {
        const { title, status, description, todo } = req.body;
        const user = new ProjectSchema(req.body);

        if (!title || !status || !description || !todo) {
          res.status(400).send({ error: 'All fields must be written' });
          return;
        }

        try {
          await user.save();
          res.send(user);
        } catch (error) {
          res.status(500).send({
            error: error.message
          });
        }
      } catch (err) {
        next(err);
      }
    });
  } catch (e) {
    console.error(err);
  }
})();
