import React, { useContext, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { UserContext } from '../../contexts/UserContext';
import '../../assets/styles/burger-menu.css';
import burgericon from '../../assets/images/burger-icon.png';
import closeicon from '../../assets/images/close-icon.png';
import projecticon from '../../assets/images/project-icon.png';
import usericon from '../../assets/images/user-icon.png';
import homeicon from '../../assets/images/home-icon.png';

const BurgerMenu = () => {
  const [displayBurgerMenu, setDisplayBurgerMenu] = useState(false);
  const { setUserConnected } = useContext(UserContext);
  const handleDisplayBurgerMenu = () => {
    setDisplayBurgerMenu(!displayBurgerMenu);
  };
  const navigate = useNavigate();
  const logout = () => {
    try {
      axios.get(`${import.meta.env.VITE_BACKEND_URL}/users/logout`, {
        withCredentials: true
      });
      navigate('/', { replace: true });
      setUserConnected(null);
      localStorage.setItem('user', null);
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant la déconnexion',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  return (
    <>
      <button
        type="button"
        className="burger-button"
        onClick={handleDisplayBurgerMenu}
      >
        <img src={burgericon} alt="burgericon" className="burger-icon" />
      </button>

      {displayBurgerMenu && (
        <div className="burger-menu">
          <ul>
            <li className="top-menu-burger">
              Menu
              <button
                type="button"
                className="close-burger-button"
                onClick={handleDisplayBurgerMenu}
              >
                <img src={closeicon} alt="closeicon" className="menu-icon" />
              </button>
            </li>
            <li>
              <img src={homeicon} alt="homeicon" className="menu-icon" />
              <Link to="/home">Home</Link>
            </li>
            <li>
              <img src={usericon} alt="usericon" className="menu-icon" />
              <Link to="/users">Users</Link>
            </li>
            <li>
              <img src={projecticon} alt="projecticon" className="menu-icon" />
              <Link to="/projects">Projects</Link>
            </li>
          </ul>
          <button type="button" onClick={logout} className="logout-button">
            Déconnexion
          </button>
        </div>
      )}
    </>
  );
};

export default BurgerMenu;
