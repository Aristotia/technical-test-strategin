import React from 'react';
import PropTypes from 'prop-types';

const ProjectModification = ({
  handlePutProject,
  projectValues,
  setProjectValues
}) => {
  ProjectModification.propTypes = {
    handlePutProject: PropTypes.func.isRequired,
    projectValues: PropTypes.shape({
      status: PropTypes.string,
      description: PropTypes.string,
      todo: PropTypes.string
    }).isRequired,
    setProjectValues: PropTypes.shape({}).isRequired
  };
  return (
    <form onSubmit={handlePutProject} className="form-put-project">
      <div className="project-item">
        <h3>Status: </h3>
        <label htmlFor="status">
          <select
            name="status"
            id="projectStatus"
            value={projectValues.status}
            onChange={(e) =>
              setProjectValues({ ...projectValues, status: e.target.value })
            }
          >
            <option value="Active">Active</option>
            <option value="Archived">Archived</option>
            <option value="Deleted">Deleted</option>
          </select>
        </label>
      </div>
      <div className="project-item">
        <h3>Description: </h3>
        <label htmlFor="description">
          <input
            type="text"
            name="description"
            id="projectDescription"
            onChange={(e) =>
              setProjectValues({
                ...projectValues,
                description: e.target.value
              })
            }
          />
        </label>
      </div>
      <div className="project-item">
        <h3>todo: </h3>
        <label htmlFor="todo">
          <input
            type="text"
            name="todo"
            id="projecttodo"
            onChange={(e) =>
              setProjectValues({ ...projectValues, todo: e.target.value })
            }
          />
        </label>
      </div>
      <input type="submit" value="Modifier le projet" />
    </form>
  );
};

export default ProjectModification;
