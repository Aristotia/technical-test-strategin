import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import PropTypes from 'prop-types';

const NewProject = ({ handleAddProject }) => {
  const validationSchema = yup.object().shape({
    title: yup.string().required('Le titre est requis'),
    status: yup.string().required(`Le statut est requis`),
    description: yup.string().required('Une description est requise'),
    todo: yup.string().required(`Au moins une tâche est requise`)
  });
  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit } = useForm(formOptions);
  NewProject.propTypes = {
    handleAddProject: PropTypes.func.isRequired
  };
  return (
    <form
      onSubmit={handleSubmit(handleAddProject)}
      className="form-add-project"
    >
      <div className="add-project-item">
        <h3>Title: </h3>
        <label htmlFor="title">
          <input
            type="text"
            name="title"
            id="projectTitle"
            {...register('title')}
          />
        </label>
      </div>

      <div className="add-project-item">
        <h3>Status: </h3>
        <label htmlFor="status">
          <select name="status" id="projectStatus" {...register('status')}>
            <option value="Active">Active</option>
            <option value="Archived">Archived</option>
            <option value="Deleted">Deleted</option>
          </select>
        </label>
      </div>
      <div className="add-project-item">
        <h3>Description: </h3>
        <label htmlFor="description">
          <input
            type="text"
            name="description"
            id="projectDescription"
            {...register('description')}
          />
        </label>
      </div>
      <div className="add-project-item">
        <h3>todo: </h3>
        <label htmlFor="todo">
          <input
            type="text"
            name="todo"
            id="projecttodo"
            {...register('todo')}
          />
        </label>
      </div>
      <input type="submit" value="Créer le projet" id="create-project-button" />
    </form>
  );
};

export default NewProject;
