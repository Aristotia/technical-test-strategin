import React from 'react';
import '../../assets/styles/header-footer.css';

const Footer = () => (
  <footer className="App-header-footer">
    <h3>Merci pour la chance donnée !</h3>
  </footer>
);

export default Footer;
