import React, { useContext } from 'react';
import { UserContext } from '../../contexts/UserContext';
import BurgerMenu from '../Unique/BurgerMenu';
import logostrategin from '../../assets/images/logo-strategin.png';
import '../../assets/styles/header-footer.css';

const Header = () => {
  const { userConnected } = useContext(UserContext);

  return (
    <header className="header-container">
      {userConnected ? (
        <>
          <BurgerMenu />
          <img
            src={logostrategin}
            alt="logostrategin"
            className="logo-strategin"
          />
          <h1>{`Bienvenue ${userConnected.email}`}</h1>
        </>
      ) : (
        <>
          <img
            src={logostrategin}
            alt="logostrategin"
            className="logo-strategin"
          />
          <h1>Mon test technique pour Strateg.in</h1>
        </>
      )}
    </header>
  );
};

export default Header;
