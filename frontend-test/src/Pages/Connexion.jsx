import axios from 'axios';
import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { UserContext } from '../contexts/UserContext';
import ConnexionInterface from '../Components/Unique/ConnexionInterface';
import Header from '../Components/Commons/Header';
import RegisterInterface from '../Components/Unique/RegisterInterface';
import '../assets/styles/connexion.css';
import Footer from '../Components/Commons/Footer';

const Connexion = () => {
  const [display, setDisplay] = useState(true);
  const { setUserConnected } = useContext(UserContext);

  const navigate = useNavigate();
  const handleInterfaceDisplay = () => {
    setDisplay(!display);
  };

  const handleRegister = async (data) => {
    try {
      await axios.post(
        `${import.meta.env.VITE_BACKEND_URL}/users/register`,
        data
      );
      Swal.fire({
        title: 'Success!',
        text: 'Enregistrement réussi',
        icon: 'success',
        confirmButtonText: 'Retour'
      });
    } catch (error) {
      Swal.fire({
        title: 'Error!',
        text: "Erreur dans l'enregistrement",
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  const handleConnexion = async (data) => {
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_BACKEND_URL}/users/login`,
        data
      );
      setUserConnected(response.data);
      localStorage.setItem('user', JSON.stringify(response.data));
      navigate('/users', { replace: true });
    } catch (error) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant la connexion',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  return (
    <div className="App">
      <Header />
      <main className="connexion-page">
        <div className="interface-container">
          <div className="button-container">
            <button type="button" onClick={handleInterfaceDisplay}>
              <h3>Se connecter</h3>
            </button>
            <button type="button" onClick={handleInterfaceDisplay}>
              <h3>Créer un compte</h3>
            </button>
          </div>
          {display ? (
            <ConnexionInterface handleConnexion={handleConnexion} />
          ) : (
            <RegisterInterface handleRegister={handleRegister} />
          )}
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default Connexion;
