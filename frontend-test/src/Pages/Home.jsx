import React from 'react';
import Footer from '../Components/Commons/Footer';
import Header from '../Components/Commons/Header';

const Home = () => (
  <div className="home-page">
    <Header />

    <Footer />
  </div>
);

export default Home;
