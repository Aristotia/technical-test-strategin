import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import Footer from '../Components/Commons/Footer';
import Header from '../Components/Commons/Header';
import '../assets/styles/userpage.css';
import binicon from '../assets/images/bin-icon.png';
import editicon from '../assets/images/edit-icon.png';

const UserManagement = () => {
  const [dataUsers, setDataUsers] = useState();
  const [userId, setUserId] = useState();
  const [userValues, setUserValues] = useState({
    firstName: '',
    lastName: '',
    email: ''
  });
  const [displayUserModification, setDisplayUserModification] = useState(false);

  const handleDisplayUserModification = (id) => {
    setDisplayUserModification(!displayUserModification);
    setUserId(id);
  };

  useEffect(() => {
    try {
      const getUsers = async () => {
        const response = await axios.get(
          `${import.meta.env.VITE_BACKEND_URL}/users`
        );
        setDataUsers(response.data);
      };
      getUsers();
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant le chargement des utilisateurs',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  }, []);

  const handleDeleteUser = async (id) => {
    try {
      await axios.delete(`${import.meta.env.VITE_BACKEND_URL}/users/${id}`);
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: "Erreur durant la suppression de l'utilisateur",
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  const handlePutUser = async () => {
    try {
      await axios.put(
        `${import.meta.env.VITE_BACKEND_URL}/users/${userId}`,
        userValues
      );
      return;
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: "Erreur dans la modification de l'utilisateur",
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };
  return (
    <div className="UserManagement">
      <Header />
      <div className="users-page">
        <h2>Utilisateurs enregistrés</h2>
        <div className="users-container">
          {dataUsers &&
            dataUsers.map((user) => (
              <div className="user-item-container" key={user._id}>
                <div className="item-button-container">
                  <button
                    type="button"
                    className="item-button"
                    onClick={() => handleDeleteUser(user._id)}
                  >
                    <img src={binicon} alt="bin-icon" className="menu-icon" />
                  </button>
                  <button
                    type="button"
                    className="item-button"
                    onClick={() => handleDisplayUserModification(user._id)}
                  >
                    <img src={editicon} alt="edit-icon" className="menu-icon" />
                  </button>
                </div>

                {displayUserModification ? (
                  <form
                    onSubmit={handlePutUser}
                    className="user-item user-put-form"
                  >
                    <h3>Prénom: </h3>
                    <label htmlFor="firstName">
                      <input
                        type="text"
                        name="firstName"
                        id="registerFirstname"
                        value={userValues.firstName}
                        onChange={(e) =>
                          setUserValues({
                            ...userValues,
                            firstName: e.target.value
                          })
                        }
                        placeholder={user.firstName}
                      />
                    </label>
                    <h3>Nom: </h3>
                    <label htmlFor="lastName">
                      <input
                        type="text"
                        name="lastName"
                        id="registerLastName"
                        value={userValues.lastName}
                        onChange={(e) =>
                          setUserValues({
                            ...userValues,
                            lastName: e.target.value
                          })
                        }
                        placeholder={user.lastName}
                      />
                    </label>
                    <h3>Email: </h3>
                    <label htmlFor="email">
                      <input
                        type="email"
                        name="email"
                        id="registerEmail"
                        value={userValues.email}
                        onChange={(e) =>
                          setUserValues({
                            ...userValues,
                            email: e.target.value
                          })
                        }
                        placeholder={user.email}
                      />
                    </label>
                    <input
                      type="submit"
                      value="Enregistrer les modifications"
                      id="inputSubmit"
                    />
                  </form>
                ) : (
                  <div className="user-item" key={user.email}>
                    <h3>
                      {user.firstName} {user.lastName}
                    </h3>
                    <h3>{user.email}</h3>
                  </div>
                )}
              </div>
            ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default UserManagement;
