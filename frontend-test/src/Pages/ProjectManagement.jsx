import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import Footer from '../Components/Commons/Footer';
import Header from '../Components/Commons/Header';
import NewProject from '../Components/Unique/NewProject';
import '../assets/styles/projectpage.css';
import addProjectIcon from '../assets/images/add-project-icon.png';
import projectMenuIcon from '../assets/images/project-menu-icon.png';
import binicon from '../assets/images/bin-icon.png';
import editicon from '../assets/images/edit-icon.png';
import ProjectModification from '../Components/Unique/ProjectModification';

const ProjectManagement = () => {
  const [dataProjects, setDataProjects] = useState();
  const [displayNewProject, setDisplayNewProject] = useState(false);
  const [displayProjectMenu, setDisplayProjectMenu] = useState(false);
  const [projectId, setProjectId] = useState();
  const [projectValues, setProjectValues] = useState({
    status: '',
    description: '',
    todo: ''
  });
  const [displayProjectModification, setDisplayProjectModification] =
    useState(false);

  const handleDisplayAddProject = () => {
    setDisplayNewProject(!displayNewProject);
  };

  const handleDisplayProjectMenu = () => {
    setDisplayProjectMenu(!displayProjectMenu);
  };

  const handleDisplayProjectModification = (id) => {
    setDisplayProjectModification(!displayProjectModification);
    setProjectId(id);
  };

  useEffect(() => {
    try {
      const getProjects = async () => {
        const response = await axios.get(
          `${import.meta.env.VITE_BACKEND_URL}/projects`
        );
        setDataProjects(response.data);
      };
      getProjects();
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant le chargement des projets',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  }, [displayNewProject]);

  const handleAddProject = async (data) => {
    try {
      await axios.post(`${import.meta.env.VITE_BACKEND_URL}/projects`, data);
      setDisplayNewProject(!displayNewProject);
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: "Erreur durant l'ajout du projet",
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  const handleDeleteProject = async (id) => {
    try {
      await axios.delete(`${import.meta.env.VITE_BACKEND_URL}/projects/${id}`);
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant la suppression du projet',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  const handlePutProject = async () => {
    try {
      await axios.put(
        `${import.meta.env.VITE_BACKEND_URL}/projects/${projectId}`,
        projectValues
      );
    } catch (err) {
      Swal.fire({
        title: 'Error!',
        text: 'Erreur durant la modification du projet',
        icon: 'error',
        confirmButtonText: 'Retour'
      });
    }
  };

  return (
    <div className="project-page">
      <Header />
      <div className="top-project-page">
        <button
          type="button"
          onClick={handleDisplayAddProject}
          className="add-project-button"
        >
          <img src={addProjectIcon} alt="add-project" className="menu-icon" />
        </button>
      </div>
      {displayNewProject ? (
        <NewProject handleAddProject={handleAddProject} />
      ) : null}
      {dataProjects &&
        dataProjects.map((project) => (
          <div className="project-container" key={project._id}>
            <div className="top-project-container">
              <h2 className="project-item">{project.title}</h2>
              <div className="project-item">{project.status}</div>
              <button
                type="button"
                onClick={handleDisplayProjectMenu}
                className="project-menu-button"
              >
                <img
                  src={projectMenuIcon}
                  alt="project-menu"
                  className="menu-icon"
                />
              </button>
              {displayProjectMenu && (
                <div className="project-menu">
                  <button
                    type="button"
                    className="item-button"
                    onClick={() => handleDeleteProject(project._id)}
                  >
                    <img src={binicon} alt="bin-icon" className="menu-icon" />
                  </button>
                  <button
                    type="button"
                    className="item-button"
                    onClick={() =>
                      handleDisplayProjectModification(project._id)
                    }
                  >
                    <img src={editicon} alt="edit-icon" className="menu-icon" />
                  </button>
                </div>
              )}
            </div>

            {displayProjectModification ? (
              <ProjectModification
                project={project}
                handlePutProject={handlePutProject}
                projectValues={projectValues}
                setProjectValues={setProjectValues}
              />
            ) : (
              <>
                <div className="project-description">{project.description}</div>
                <div className="project-todo">{project.todo}</div>
              </>
            )}
          </div>
        ))}
      <Footer />
    </div>
  );
};

export default ProjectManagement;
