import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import UserContextProvider from './contexts/UserContext';
import Connexion from './Pages/Connexion';
import UserManagement from './Pages/UserManagement';
import Home from './Pages/Home';
import ProjectManagement from './Pages/ProjectManagement';
import './assets/styles/global.css';

const App = () => (
  <div>
    <UserContextProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Connexion />} />
          <Route path="/home" element={<Home />} />
          <Route path="/users" element={<UserManagement />} />
          <Route path="/projects" element={<ProjectManagement />} />
        </Routes>
      </Router>
    </UserContextProvider>
  </div>
);

export default App;
